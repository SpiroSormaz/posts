import { createRouter, createWebHistory } from 'vue-router'
import Home from '../views/Home.vue'
import Comments from '../views/Comments.vue'
import Posts from '../views/Posts.vue'
import NotFound from '../views/NotFound.vue'

const routes = [
  {
    path: '/',
    name: 'Home',
    component: Home
  },
  {
    path: '/posts',
    name: 'Posts',
    component: Posts
  },
  {
    path: '/post/:id',
    name: 'Comments',
    component: Comments
  },
 
  //catch 404
  {
    path:'/:catchAll(.*)',
    name:'NotFound',
    component: NotFound,
  }
]

const router = createRouter({
  history: createWebHistory(process.env.BASE_URL),
  routes
})

export default router
