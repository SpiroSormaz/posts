
//This is only for example , we  use mixins if we need the same functionality in many components.
export default {
   computed:{
    filterPosts(){
        return this.transformatedPosts.filter(post => post.name.toLowerCase().includes(this.searchData.toLowerCase()))
  }
   }
}